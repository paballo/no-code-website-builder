import { useEffect , useState} from 'react';
import axios from 'axios';
import React from 'react';
import { Link} from 'react-router-dom';
import Form from "react-bootstrap/Form";
import "./template.css";
import { FaCheck, FaEye } from 'react-icons/fa';

function Templates() {
    const [results, setResults] = useState([]);
    const [searchBarcode, setSearchBarcode] = useState("");
       useEffect(() => {
        axios.get(`/api/get-templates`)
        .then((response) => {
          setResults(response.data.data);
        });
      }, []);
    
      const filterBarcode = results.filter((item) => {
        return searchBarcode !== "" ? item.name === searchBarcode : item;;
      });
    
      const searchResults = filterBarcode.map((data, i) => {
        return (
           <div className="col-sm-4"  key={i}>
           <div className="card box">
             <div className="card-body">
               <img src={data.image} className='img-fluid template-img' srcSet='' />
               <br />
               <br />
               <h5 className="card-title">{data.name}</h5>
               <p className="card-text">With supporting text below as a natural lead-in to additional content.</p>
               <Link to={`view-template/${data.identifier}`} className="btn bg-primary text-white link-item m-2"><FaEye/> View</Link>  
               <Link to={`edit-template/${data.id}`} className="btn btn-success link-item"><FaCheck/> Customize</Link>
             </div>
           </div>
           <br></br>
         </div>
         
        );
      });
  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container">
     <a className="navbar-brand" href="#">
           <img src="" alt="" srcset="" />
      </a>
    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
    </div>
  </div>
</nav>
<br />
<Form>
        <Form.Group controlId="formGroupSearch">
          <Form.Control
            autoFocus
            type="text"
            placeholder="Search template"
            onChange={(e) => setSearchBarcode(e.target.value)}
            className="control"
          />
        </Form.Group>
      </Form>
<div className="row p-10 d-flex test">
     {searchResults}
</div>
    </div>
  )
}

export default Templates