import React, { useRef, useState  }  from 'react';
import './form.css';
import { FaArrowRight } from 'react-icons/fa';
import { useHistory } from 'react-router-dom';
import * as htmlToImage from 'html-to-image';

function ProjectName() {
  const history = useHistory();
  const domEl = useRef(null);
  const [projectInput, setProjectName] = useState({
    name : '',
  });

  const handleInput = (e) => {
    e.persist();
    setProjectName({...projectInput, [e.target.name]: e.target.value })
  }

  const ProjectNameSubmit = async (e) => {
    e.preventDefault();
    const dataUrl = await htmlToImage.toPng(domEl.current);

    // download image
    const link = document.createElement('a');

    link.href = dataUrl;
    link.click();
    localStorage.setItem('image', JSON.stringify(dataUrl));
  
    const data = {
      name: projectInput.name,
    }
  
    localStorage.setItem('name', JSON.stringify(data));
  
    const res = {
      data: {
        status: 200 
      }
    }
  
    if (res.data.status === 200) {
      history.push('/create');
    } else if (res.data.status === 401) {
      
    }
  }
  
  return (
    <div className="form">
        <form className='form__group field' onSubmit={ProjectNameSubmit}>
            <input type="text" className='input' name='name' placeholder='Project Name' onChange={handleInput} value={projectInput.name} required/>
            <br/>
            <br/>
            <div id="domEl" ref={domEl}>
        <h3>Convert HTML element or document into Image in React</h3>
        <h3>
          <a href="https://www.cluemediator.com/" target="_blank">
            Clue Mediator
          </a>
        </h3>
      </div>
            <button className='btn btn-light p-3 rounded-circle btn-lg btnname' type='submit'>< FaArrowRight/></button>
        </form>
    </div>
  )
}

export default ProjectName
